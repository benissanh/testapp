<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of User
 *
 * @author pc
 */
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="users")
 */
class User {
    /** 
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    protected $id;
    /** 
     * @ORM\Column(type="string") 
     */
    protected $login;
    /** 
     * @ORM\Column(type="string") 
     */
    protected $pwd;
    /** 
     * @ORM\Column(type="datetime") 
     */
    protected $createdDate;
    /** 
     * @ORM\Column(type="datetime") 
     */
    protected $updatedDate;
    /**
     * @ORM\ManyToOne(targetEntity="Role")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $role;
    
    public function getId()
    {
       return $this->id; 
    }
    
    public function getLogin()
    {
       return $this->login; 
    }
    
    public function setLogin($login)
    {
        $this->login = $login;
    }
    
    public function getCreatedDate()
    {
       return $this->createdDate; 
    }
    
    public function setCreatedDate($createdDate)
    {
       $this->createdDate = $createdDate; 
    }
    
    public function getUpdatedDate()
    {
        return $this->updatedDate;
    }
    
    public function setUpdatedDate($updatedDate)
    {
       $this->updatedDate = $updatedDate; 
    }
    
    public function getRole()
    {
        return $this->role;
    }
    
    public function setRole($role)
    {
      $this->role = $role;  
    }
}
