<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Task
 *
 * @author pc
 */
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="tasks")
 */
class Task {

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    protected $id;

    /**
     * @ORM\Column(type="string") 
     */
    protected $title;

    /**
     * @ORM\Column(type="string", nullable=true) 
     */
    protected $description;

    /**
     * @ORM\Column(type="datetime") 
     */
    protected $createdOn;

    /**
     * @ORM\Column(type="datetime") 
     */
    protected $startDate;

    /**
     * @ORM\Column(type="datetime") 
     */
    protected $updatedOn;

    /**
     * @ORM\Column(type="integer") 
     */
    protected $status;

    /**
     * @ORM\Column(type="boolean") 
     */
    protected $deleted;

    /**
     * @ORM\Column(type="datetime") 
     */
    protected $endDate;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $user;

    public function getId() {
        return $this->id;
    }

    public function getTitle() {
        return $this->title;
    }

    public function setTitle($title) {
        $this->title = $title;
    }

    public function getDescription() {
        return $this->description;
    }

    public function setDescription($description) {
        $this->description = $description;
    }

    public function getCreatedOn() {
        return $this->createdOn;
    }

    public function setCreatedOn(\DateTime $createdOn) {
        $this->createdOn = $createdOn;
    }

    public function getStartDate() {
        return $this->startDate;
    }

    public function setStartDate($startDate) {
        $this->startDate = $startDate;
    }

    public function getUpdatedOn() {
        return $this->title;
    }

    public function setUpdatedOn($updatedOn) {
        $this->updatedOn = $updatedOn;
    }

    public function getStatus() {
        return $this->status;
    }

    public function setStatus($status = 0) {
        $this->status = $status;
    }

    public function getDeleted() {
        return $this->deleted;
    }

    public function setDeleted($deleted = false) {
        $this->deleted = $deleted;
    }

    public function getEndDate() {
        return $this->endDate;
    }

    public function setEndDate($endDate) {
        $this->endDate = $endDate;
    }

    public function getUser() {
        return $this->user;
    }

    public function setUser($user) {
        $this->user = $user;
    }

}
