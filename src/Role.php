<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Role
 *
 * @author pc
 */
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="roles")
 */
class Role {
    /** 
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    protected $id;
    /** 
     * @ORM\Column(type="string") 
     */
    protected $title;
    /** 
     * @ORM\Column(type="datetime") 
     */
    protected $createdOn;
    /** 
     * @ORM\Column(type="datetime") 
     */
    protected $updatedOn;
    
    public function getId()
    {
       return $this->id;  
    }
    
    public function getTitle()
    {
       return $this->title;  
    }
    
    public function setTitle($title)
    {
       $this->title = $title; 
    }
    
    public function getCreatedOn()
    {
        return $this->createdOn; 
    }
    
    public function setCreatedOn($createdOn)
    {
       $this->createdOn = $createdOn; 
    }
    
    public function getUpdatedOn()
    {
       return $this->updatedOn;  
    }
    
    public function setUpdatedOn($updatedOn)
    {
      $this->updatedOn = $updatedOn;   
    }
}
